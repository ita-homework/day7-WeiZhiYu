package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.model.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class EmployeeRepository {
    private static final List<Employee> employees = new ArrayList<>();
    private static final AtomicLong atomicId = new AtomicLong(0);

    public static Employee addEmployee(Employee employee) {
        employee.setId(atomicId.incrementAndGet());
        employees.add(employee);
        return employee;
    }

    public static List<Employee> getEmployees() {
        return employees;
    }

    public static Employee getEmployeeById(Long employeeId) {
        return employees.stream()
                .filter(employee -> Objects.equals(employee.getId(), employeeId))
                .findFirst()
                .orElseThrow(RuntimeException::new);
    }

    public static List<Employee> getEmployeesByGender(String gender) {
        return employees.stream()
                .filter(employee -> Objects.equals(employee.getGender(), gender))
                .collect(Collectors.toList());
    }

    public static Employee updateAgeAndSalary(Long employeeId, Employee employee) {
        Employee updatedEmployee = getEmployeeById(employeeId);
        updatedEmployee.setAge(employee.getAge());
        updatedEmployee.setSalary(employee.getSalary());
        return updatedEmployee;
    }

    public static void deleteEmployeeById(Long employeeId) {
        employees.remove(getEmployeeById(employeeId));
    }

    public static List<Employee> queryEmployees(Integer page, Integer size) {
        return employees.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }
}
