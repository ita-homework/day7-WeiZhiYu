# O
- Code Review: Refactor
- Review OOP, TDD, Refactor: Draw three concept maps
- HTTP: It is the application-layer protocol for transferring data like: HTML page, Images, Video…
- RESTful: REST is acronym for REpresentational State Transfer. It is architectural style and was first presented by Roy Thomas Fielding in 2000 in his famous dissertation. In RESTful, every URI represents the resource. And it transfer the representational of the resource between client and server. Finally, To make the “representational state transfer”, client use HTTP verbs(GET, POST, PUT, DELETE…) to let the server process the resource.
- Spring Boot: Spring Boot makes it easy to create stand-alone, production-grade Spring based Applications that you can "just run".

# R
I'm a little nervous.
# I
I have limited experience in Web development and Spring Boot is a new technology for me, so I am still getting familiar with it.
# D
I will spend more time to study.